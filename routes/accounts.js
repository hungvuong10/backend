const express = require("express")
const router = express.Router()
const accountsController = require("../controller/accountsController")


router.post("/signup", accountsController.Signup)
router.post("/login", accountsController.Login)

module.exports = router
