    const bcrypt = require('bcrypt');
    const AccountsModel = require("../model/accounts")
    const jwt = require('jsonwebtoken');
    const saltRounds = 10;
    const AccountsController = {}



    AccountsController.Signup = async (req, res) => {

        try {
            let {username, email, password} = req.body

            const user = await AccountsModel.findOne({email})
            if(user){
                return res.status(400).json({
                    status: 400,
                    message: "Not Found"
                })
            }


            bcrypt.genSalt(saltRounds, function (err, salt) {
                bcrypt.hash(req.body.password, salt, async function (err, hash) {
                    password = hash;
                    
                    await AccountsModel.create({
                        username: username,
                        email: email,
                        password: password
                    })
                    
                    return res.status(200).json({
                        status: 200,
                        message: "Success",
                    })
                });
            });

        } catch (error) {
            return res.status(400).json({
                status: 400,
                message: "Not Found"

            })
        }
    }


    AccountsController.Login = async (req, res) => {
        try {

            let {email, password} = req.body
            let user = await AccountsModel.findOne({email})
            if (user) {
                bcrypt.compare(password, user.password, async function (err, result) {
                    if (!result || err) {
                        return res.status(400).json({
                            status: 400,
                            message: "Not Found"
                        })
                    }

                    return res.status(200).json({
                        status: 200,
                        message: "Success",
                        user: user,
                    })
                })
            } else {
                return res.status(400).json({
                    status: 400,
                    message: "Not Found"
                })
            }
        } catch (error) {
            return res.status(500).json({
                status: 500,
                message: "Error Server"
            })
        }
    }

module.exports = AccountsController